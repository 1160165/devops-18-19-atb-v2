# Jenkins pipeline demonstration

This project demonstrates how to make a pipeline on Jenkins.

Part 2 - Create pipeline to errai-demonstration
-------------
The second part is very similar to first, but we had to add more stages on Pipeline to cover more steps, including the connection of Jenkins with Docker.

The repository used was the same of part1: devops-18-19-atb-<1160165>v2. 

1) Copy CA2 - part 2 folder

The propose was following the errai-demonstration project that we already had on CA2 folder part 2. We copy this project and paste to the new repository on folder part2. 

Then we created a Jenkinsfile on folder "ca2/part2" with the instructions:

	pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'gradle-basic-demo', url: 'https://bitbucket.org/1160165/devops-18-19-atb-v2'
            }
        }
	    stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'cd part2/errai-demonstration-gradle-master && gradlew clean assemble'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                bat 'cd part2/errai-demonstration-gradle-master && gradlew test'
                junit '**\\test-results\\test\\*.xml'
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Javadoc...'
                bat 'cd part2/errai-demonstration-gradle-master && gradlew javadoc'
            }
        }
		stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
		stage('Publish image') {
            steps {
				echo 'Publishing image...'
                 script {
                    dockerImage = docker.build ("1160165/ca5-part2:$BUILD_NUMBER","part2/errai-demonstration-gradle-master").push()
					}
				}
			}
		}
	}
	
The steps were separeted in stages to do specific things:

	% Checkout: To checkout the code from the repository
	% Assemble: Compiles and Produces the archive files with the application. 
	% Test: Executes the Unit Tests and publish in Jenkins the Test results.
	% Javadoc: Generates the javadoc (including Plantuml diagrams!) and publish it in Jenkins. 
	% Archive: Archives in Jenkins the archive files (generated during Assemble, i.e., the war file)
	% Publish Image: Generate a docker image with Wildfly and the war file and publish it in the Docker Hub.
	
The most difficult thing on this part was to connect jenkins with docker. "1160165" is my id credential that I use on Dockerhub and ca5-part2 is the repository that I've created on Dockerhub. After many tries the result was the above script. I also had to create a Dockerfile with the following instructions:

	FROM ubuntu

	RUN apt-get update && \
	apt-get install -y openjdk-8-jdk-headless && \
	apt-get install unzip -y && \
	apt-get install wget -y

	RUN mkdir -p /usr/src/app

	WORKDIR /usr/src/app/

	RUN wget http://download.jboss.org/wildfly/15.0.1.Final/wildfly-15.0.1.Final.tar.gz && \
	tar -xvzf wildfly-15.0.1.Final.tar.gz && \
	sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
	sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
	sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml

	COPY build/libs/errai-demonstration-gradle-master.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/

	CMD /usr/src/app/wildfly-15.0.1.Final/bin/standalone.sh


We had to move the errai-demonstration-gradle-master.war to folder build/libs to copy this file to deployments.

Credentials were defined on Jenkins with the id "gradle-basic-demo" and the url of repository to connect with bitbucket is
"https://bitbucket.org/1160165/devops-18-19-atb-v2" 

Next, we had to run jenkins.war and on url "localhost:8080" and create a new job pipeline.

	From the main page of Jenkins select "New Item"
	For the name enter "example"
	Select "Pipeline"
	Click on "OK"

In the configuration select Pipeline script from SCM instead of Pipeline script
I had selected git and enter the URL of the repository and select the Credentials.

When configuring the pipeline, in the Script Path field, I specified the relative path for the Jenkinsfile. For instance, ca2/part2/errai-demonstration-gradle-master/Jenkinsfile.

After that was necessary to click on Build Now to execute the pipeline
Jenkins executed the pipeline as specified in the Jenkinsfile inside the repository

The image gerated was automatically moved to Dockerhub on repository ca5-part2 with the tag "17" that refers to number of tries of buildings of pipeline.
https://cloud.docker.com/repository/docker/1160165/ca5-part2

To finish the step I created a new tag "ca5-part2", and commited the Jenkinsfile with the project.
