# Emular App Anbima android local com simula��o de dados

Para emular o app localmente e simular a inser��o de dados � necess�rio seguir alguns pr� requisitos.

1) Criar um servidor local php e MySQL

Para simular um servidor utilizamos no exemplo o Wamp que subir� um servidor para o php e outro que ser� utilizado para o MySQL. 

2) Importar dados da base desejada

Em http://localhost/phpmyadmin/ devemos primeiramente criar uma base de dados 'anb2' e nela importar os dados previamente de outro banco de dados 

	- http://10.225.66.115:90/phpmyadmin ( Homol )
	- http://10.224.40.13:90/phpmyadmin ( Produ��o )
	
	Usuario e senha para acesso ao BD MySQL:
	usuario: consultas
	senha: digiOceanDBPass1425

3) Configurar o backend do aplicativo (http://tools.anbima.com.int:8888/representacao/app-anbima-backend)

Na pasta \wamp64\www colocamos o backend do aplicativo em php e para acessar os dados localmente devemos fazer algumas altera��es em alguns arquivos:

	> \wamp64\www\app-anbima-backend\connect.php
	
	$usr = 'root';//usu�rio definido por padr�o pelo wamp
	$pass = '';//senha padr�o definida pelo wamp
	$baseDados = 'anb2';//Nome da base 
	$servidor = '127.0.0.1';//
	//conecto com o servidor mysql e recebo uma variavel com a conex�o com essa conex�o eu posso gravar e ler o banco de dados


	> \wamp64\www\app-anbima-backend\_READ\headerRead_mysql.php
	
	$usuario = 'root';
	$senha = '';
	$baseDados = 'anb2';
	$servidor = '127.0.0.1';
	$con =  mysqli_connect($servidor,$usuario, $senha, $baseDados) or die(mysqli_connect_error()); 


	> \wamp64\www\app-anbima-backend\_READ\leTxD_mysql.php
	
	chdir('C:\wamp64\www\app-anbima-backend\_READ\txt'); // caminho onde ser�o colocados os arquivos txt com os dados desejadados de inser��o. 
	Obs: depois da leitura os arquivos ir�o para uma pasta definida por padr�o "historico", ent�o deve-se criar localmente essa pasta dentro do diret�rio de leitura dos arquivos, para n�o ocorrer nenhum erro.
	
Com essas altera��es j� � poss�vel fazer chamadas locais para realizar a inser��o de dados no banco de dados local, pelo endere�o:

	http://localhost:8080/app-anbima-backend/_READ/parseTxtsD_mysql.php

O arquivo vem no formato txt e a seguir um exemplo de uma linha que ser� inserida no banco de dados

	201909302019100301                                        Renda Fixa                                Renda Fixa Simples                                           Simples                                            Simples                                                                                                                                              O tipo RF Simples iniciou em 01/10/2015. As informa��es de rentabilidade anteriores a esta data foram estimadas.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        Renda Fixa : Fundos que t�m como objetivo buscar retorno por meio de investimentos em ativos de renda fixa (sendo aceitos t�tulos sintetizados atrav�s do uso de derivativos), admitindo-se estrat�gias que impliquem risco de juros e de �ndice de pre�os. S�o admitidos ativos de renda fixa emitidos no exterior. Excluem-se estrat�gias que impliquem exposi��o de moeda estrangeira ou de renda vari�vel (a��es, etc.).<br>Estes fundos seguem o disposto no art. 113 da Instru��o n� 555/14 da CVM     27515,25     27763,94         0,53         0,02         0,38         3,89       -38,48       148,52      2330,491

Sendo:

(1)20190930(2)20191003(3)01(4)Renda Fixa(5)Renda Fixa Simples(6)Simples(7)Simples(8)O tipo RF Simples iniciou em 01/10/2015. As informa��es de rentabilidade anteriores a esta data foram estimadas.(9)Renda Fixa : Fundos que t�m como objetivo buscar retorno por meio de investimentos em ativos de renda fixa (sendo aceitos t�tulos sintetizados atrav�s do uso de derivativos), admitindo-se estrat�gias que impliquem risco de juros e de �ndice de pre�os. S�o admitidos ativos de renda fixa emitidos no exterior. Excluem-se estrat�gias que impliquem exposi��o de moeda estrangeira ou de renda vari�vel (a��es, etc.).<br>Estes fundos seguem o disposto no art. 113 da Instru��o n� 555/14 da CVM(10)27515,25(11)27763,94(12)0,53(13)0,02(14)0,38(15)3,89(16)-38,48(17)148,52(18)2330,49(19)1

	(1)Data de referencia
	(2)Data de emiss�o
	(3)Tipo do registro. � aqui que � definido se � um tipo de nivel 1, indice, categoria ou uma categoria do indice.
		01 - Tipos
		02 - Indices
		50 - Categoria fundos
		51 - Totalizador fundos
		52 - Categoria fundos
		60 - Totalizador nivel 1
	(4)Nome do n�vel 1
	(5)Nome da categoria
	(6)Tipo
	(7)Subcategoria
	(8)Observa��o
	(9)Cartilha
	(10)plMesAnt
	(11)plNaData
	(12)plPctTot
	(13)rentDia 
	(14)rentMes 
	(15)rentAno 
	(16)clDia 
	(17)clMes
	(18)clAno
	(19)1 se est� ativo e 0 n�o ativo
	
4)Emular o app para verificar as altera��es (http://tools.anbima.com.int:8888/representacao/app-anbima-backend)

Para emular o aplicativo utilizamos o Android Studio e para testar localmente as altera��es devemos realizar algumas altera��es.

Na classe WebserviceUtil:

    public static final String GET_DIARIOS = "http://localhost:8080/app-anbima-backend/_APP/GetDiarios_mysql.php"; 
	
    public static final String GET_NOTICIAS = "http://localhost:8080/app-anbima-backend/_APP/GetNoticias_mysql.php?idLastNews=@param"; 

    public static final String GET_GRAFICOS = "http://localhost:8080/app-anbima-backend/_APP/GetGraficos_mysql.php?idTipo=@param";
	
    public static final String POST_RELATORIO = "http://localhost:8080/app-anbima-backend/_APP/InsertRelat_mysql.php";

Feito isso basta correr o app utilizando o emulador desejado e verificar as altera��es.


	

